(function() {
    "use strict";
    
    // Initialize The Service Worker.
    if(navigator.serviceWorker) {
        // Register a Service Worker hosted at the root of the site using the default scope.
        navigator.serviceWorker.register('/serviceworker.js').then((registration) => {
            // Registration was successful.
            console.log('El registro del Service Worker se realizó correctamente: ', registration.scope);
        });
    } else {
        //The Service Worker isn't supported on this browser
        console.error('Este navegador no es compatible con el Service Worker.');
    };
})()
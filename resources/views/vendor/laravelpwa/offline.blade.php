@extends('layouts.sesion')

@section('content')
    <span class="padding-bottom--15">{{ __('Desconectado') }}</span>
    <center>
        <img src="{{ asset('img/offline/offline.jpg') }}" class="img-fluid mb-4" width="250" height="250" alt="Offline">
    </center>
    <a href="@auth {{ route('home') }} @else {{ url('/') }} @endauth" class="field padding-bottom--24">
        <input type="submit" name="recargar" value="Recargar">
    </a>
@endsection
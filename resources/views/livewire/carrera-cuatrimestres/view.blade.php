@section('title', __('Carreras/Cuatrimestres'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col mb-2">
							<a style="text-decoration: none; color: black;" href="{{ url('/carreras') }}">{{ __('Carreras') }}</a>{{ __('/') }}<b>{{ __('Cuatrimestres') }}</b>
						</div>
					</div>
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/cuatrimestres.png') }}" alt="sensores">
								{{ __('Cuatrimestres') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar cuatrimestre para las carreras">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($carreras->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCarrera" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una carrera') }}</option>
										@foreach($carreras as $item)
											<option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
										@endforeach
									</select>
									<label for="filtroCarrera">{{ __('Carreras') }}</label>
								</div>
							@endif
							@if($cuatrimestres->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCuatrimestre" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un cuatrimestre') }}</option>
										@foreach($cuatrimestres as $item)
											<option value="{{ $item->id }}">{{ $item->cuatrimestre }}</option>
										@endforeach
									</select>
									<label for="filtroCuatrimestre">{{ __('Cuatrimestres') }}</label>
								</div>
							@endif
						</div>
					</div>
				</div>

				@include('livewire.carrera-cuatrimestres.form')

				@if($carreraCuatrimestres->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Periodo cuatrimestral') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($carreraCuatrimestres as $row)
									<tr>
										<td scope="row">{{ $row->carrera->grado->abreviatura }} {{ __('en') }} {{ $row->carrera->carrera }}</td>
										<td scope="row">{{ $row->cuatrimestre->cuatrimestre }}</td>
										<td scope="row">{{ $row->cuatrimestre->periodo_cuatrimestral_inicial }} {{ __('---') }} {{ $row->cuatrimestre->periodo_cuatrimestral_final }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{$row->id}})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el cuatrimestre {{ $row->cuatrimestre->cuatrimestre }} de la carrera de {{ $row->carrera->carrera }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el cuatrimestre {{ $row->cuatrimestre->cuatrimestre }} de la carrera de {{ $row->carrera->carrera }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $carreraCuatrimestres->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="carrera_id" class="form-select bg-light text-dark border-0 @error('carrera_id') is-invalid @enderror" required>
                @if($carreras->count())
                    <option selected>{{ __('Selecciona una carrera') }}</option>
                    @foreach($carreras as $item)
                        <option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ninguna carrera') }}</option>
                @endif
            </select>
            <label for="carrera_id" class="text-dark">{{ __('Carreras') }}</label>
            @error('carrera_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if ($identificador < 1)
            <div class="mb-4">
                <label for="cuatrimestre_id" class="text-dark">{{ __('Cuatrimestres') }}</label>
                <select wire:model.lazy="cuatrimestre_id" multiple class="form-select bg-light text-dark border-0 @error('cuatrimestre_id') is-invalid @enderror" required>
                    @if ($cuatrimestres->count())
                        @foreach($cuatrimestres as $item)
                            <option value="{{ $item->id }}">{{ $item->cuatrimestre }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ningun cuatrimestre') }}</option>
                    @endif
                </select>
                @error('cuatrimestre_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @else
            <div class="form-floating mb-4">
                <select wire:model.lazy="cuatrimestre_id" class="form-select bg-light text-dark border-0 @error('cuatrimestre_id') is-invalid @enderror" required>
                    @if ($cuatrimestres->count())
                        @foreach($cuatrimestres as $item)
                            <option value="{{ $item->id }}">{{ $item->cuatrimestre }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ningun cuatrimestre') }}</option>
                    @endif
                </select>
                <label for="cuatrimestre_id" class="text-dark">{{ __('Cuatrimestres') }}</label>
                @error('cuatrimestre_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
    </div>
</form>
@include('modal.footer')
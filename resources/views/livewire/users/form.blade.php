@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="fp" type="file" accept="image/*" class="form-control bg-light text-dark border-0 @error('fp') is-invalid @enderror" id="{{ rand() }}" required>
            <label for="fp" class="text-dark">{{ __('Foto de perfil') }}</label>
            @error('fp')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if ($fp && $identificador < 1)
        <div class="col-lg-6 mb-4 text-center">
            <img  src="{{ $fp->temporaryUrl() }}" class="img-fluid rounded-circle" alt="foto" width="110" height="110">            
        </div>
        @elseif ($fp && $identificador > 0)
        <div class="col-lg-6 mb-4 text-center">
            <img src="{{ $fp == $tmp ? asset('').$fp : $fp->temporaryUrl() }}" class="img-fluid rounded-circle" alt="{{ $username }}" width="110" height="110">
        </div>
        @endif
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="nombre" type="text" class="form-control bg-light text-dark border-0 @error('nombre') is-invalid @enderror" placeholder="Ingresa un nombre" required autocomplete="nombre">
            <label for="nombre" class="text-dark">{{ __('Nombre') }}</label>
            @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="ap" type="text" class="form-control bg-light text-dark border-0 @error('ap') is-invalid @enderror" placeholder="Ingresa un apellido paterno" required autocomplete="ap">
            <label for="ap" class="text-dark">{{ __('Apellido paterno') }}</label>
            @error('ap')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="am" type="text" class="form-control bg-light text-dark border-0 @error('am') is-invalid @enderror" placeholder="Ingresa un apellido materno" required autocomplete="am">
            <label for="apellido_materno" class="text-dark">{{ __('Apellido materno') }}</label>
            @error('am')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <select wire:model.lazy="genero_id" class="form-select bg-light text-dark border-0 @error('genero_id') is-invalid @enderror" required>
                <option selected>{{ __('Selecciona un género') }}</option>
                @foreach($generos as $item)
                    <option value="{{$item->id}}">{{$item->genero}}</option>
                @endforeach
            </select>
            <label for="genero_id" class="text-dark">{{ __('Genero') }}</label>
            @error('genero_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="email" type="email" class="form-control bg-light text-dark border-0 @error('email') is-invalid @enderror" placeholder="Ingresa un email valido" required autocomplete="email">
            <label for="email" class="text-dark">{{ __('Email') }}</label>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="username" type="text" class="form-control bg-light text-dark border-0 @error('username') is-invalid @enderror" placeholder="Ingresa una matricula" required autocomplete="username">
            <label for="username" class="text-dark">{{ __('Matricula') }}</label>
            @error('username')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="password" type="password" class="form-control bg-light text-dark border-0 @error('password') is-invalid @enderror" placeholder="********" required autocomplete="password">
            <label for="password" class="text-dark">{{ __('Contraseña') }}</label>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <select wire:model.lazy="rol" class="form-select bg-light text-dark border-0 @error('rol') is-invalid @enderror" required>
                <option selected>{{ __('Selecciona un rol') }}</option>
                @foreach($roles as $item)
                    @if ($item->guard_name == "web")
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endif
                @endforeach
            </select>
            <label for="rol" class="text-dark">{{ __('Roles') }}</label>
            @error('rol')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if ($identificador > 0)
            <div class="form-floating col-lg-6 mb-4">
                <select wire:model.lazy="estatu_usuario_id" class="form-select bg-light text-dark border-0 @error('estatu_usuario_id') is-invalid @enderror" required>
                    <option selected>{{ __('Selecciona un estatu') }}</option>
                    @foreach($modos as $item)
                        <option value="{{$item->id}}">{{$item->estatu}}</option>
                    @endforeach
                </select>
                <label for="estatu_usuario_id" class="text-dark">{{ __('Estatus') }}</label>
                @error('estatu_usuario_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
    </div>
</form>
@include('modal.footer')
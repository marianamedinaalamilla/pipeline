@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="user_id" class="form-select bg-light text-dark border-0 @error('user_id') is-invalid @enderror" required>
                @if($users->count())
                    <option selected>{{ __('Selecciona un usuario') }}</option>
                    @foreach($users as $item)
                        @foreach ($item->roles as $item2)
                            @if($item2->name == "Estudiante" && $item2->guard_name == "web")
                                <option value="{{ $item->id }}">{{ $item->apellido_paterno }} {{ __(' ') }} {{ $item->apellido_materno }} {{ __(' ') }} {{ $item->nombre }}</option>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun usuario') }}</option>
                @endif
            </select>
            <label for="sensor_id" class="text-dark">{{ __('Usuarios') }}</label>
            @error('user_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <select wire:model.lazy="carrera_id" class="form-select bg-light text-dark border-0 @error('carrera_id') is-invalid @enderror" required>
                @if($carreras->count())
                    <option selected>{{ __('Selecciona una carrera') }}</option>
                    @foreach($carreras as $item)
                        <option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ninguna carrera') }}</option>
                @endif
            </select>
            <label for="sensor_id" class="text-dark">{{ __('Carreras') }}</label>
            @error('carrera_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="identificacion" type="text" class="form-control bg-light text-dark border-0 @error('identificacion') is-invalid @enderror" placeholder="Ingresa un identificación" required autocomplete="identificacion">
            <label for="identificacion" class="text-dark">{{ __('Identificación') }}</label>
            @error('identificacion')
            <span class="invalid-feedback" role="alert">
                <label>{{ $message }}</label>
            </span>
            @enderror
        </div>
    </div>
</form>
@include('modal.footer')
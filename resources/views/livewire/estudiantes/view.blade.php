@section('title', __('Estudiantes'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bi bi-person-badge icon"></i>
								{{ __('Estudiantes') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar estudiante">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($users->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroUser" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un usuario') }}</option>
										@foreach($users as $item)
											<option value="{{ $item->id }}">{{ $item->apellido_paterno }} {{ __(' ') }} {{ $item->apellido_materno }} {{ __(' ') }} {{ $item->nombre }}</option>
										@endforeach
									</select>
									<label for="filtroUser">{{ __('Usuarios') }}</label>
								</div>
							@endif
							@if($carreras->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCarrera" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una carrera') }}</option>
										@foreach($carreras as $item)
											<option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
										@endforeach
									</select>
									<label for="filtroCarrera">{{ __('Carreras') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar identificación">
								<label for="buscador">{{ __('Buscar identificación') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.estudiantes.form')

				@if($estudiantes->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto') }}</th>
										<th scope="col">{{ __('Nombre completo') }}</th>
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Identificación') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($estudiantes as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->user->foto_perfil) }}" alt="{{ $row->user->username }}" width="110" height="110"></td>
										<td scope="row">{{$row->user->apellido_paterno}} {{ __(' ') }} {{$row->user->apellido_materno}} {{ __(' ') }} {{ $row->user->nombre }}</td>
										<td scope="row">{{$row->carrera->grado->abreviatura}} {{ __('en') }} {{ $row->carrera->carrera }}</td>
										<td scope="row">{{ $row->identificacion }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar al estudiante {{$row->apellido_paterno}} {{ __(' ') }} {{$row->apellido_materno}} {{ __(' ') }} {{ $row->nombre }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar al estudiante {{$row->apellido_paterno}} {{ __(' ') }} {{$row->apellido_materno}} {{ __(' ') }} {{ $row->nombre }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $estudiantes->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
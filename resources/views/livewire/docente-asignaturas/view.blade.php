@section('title', __('Docentes/Asignaturas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col mb-2">
							<a style="text-decoration: none; color: black;" href="{{ url('/docentes') }}">{{ __('Docentes') }}</a>{{ __('/') }}<b>{{ __('Asignaturas') }}</b>
						</div>
					</div>
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-book icon"></i>
								{{ __('Asignaturas') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar asignatura para los docentes">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($docentes->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroDocente" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un docente') }}</option>
										@foreach($docentes as $item)
											<option value="{{ $item->id }}">{{$item->user->apellido_paterno}} {{ __(' ') }} {{$item->user->apellido_materno}} {{ __(' ') }} {{ $item->user->nombre }}</option>
										@endforeach
									</select>
									<label for="filtroDocente">{{ __('Docentes') }}</label>
								</div>
							@endif
							@if($cuatrimestres->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCuatrimestre" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un cuatrimestre') }}</option>
										@foreach($cuatrimestres as $item)
											<option value="{{ $item->id }}">{{ $item->cuatrimestre }}</option>
										@endforeach
									</select>
									<label for="filtroCuatrimestre">{{ __('Cuatrimestres') }}</label>
								</div>
							@endif
							@if($asignaturas->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroAsignatura" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una asignatura') }}</option>
										@foreach($asignaturas as $item)
											<option value="{{ $item->id }}">{{ $item->asignatura }}</option>
										@endforeach
									</select>
									<label for="filtroAsignatura">{{ __('Asignaturas') }}</label>
								</div>
							@endif
							@if($grupos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGrupo" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un grupo') }}</option>
										@foreach($grupos as $item)
											<option value="{{ $item->id }}">{{ $item->grupo }}</option>
										@endforeach
									</select>
									<label for="filtroGrupo">{{ __('Grupos') }}</label>
								</div>
							@endif
							@if($dias->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroDia" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un día') }}</option>
										@foreach($dias as $item)
											<option value="{{ $item->id }}">{{ $item->dia }}</option>
										@endforeach
									</select>
									<label for="filtroDia">{{ __('Días') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='filtroHora' class="form-control bg-light text-dark border-0" type="time" placeholder="Buscar hora">
								<label for="filtroHora">{{ __('Buscar hora') }}</label>
							</div>
						</div>
					</div>
				</div>

				@include('livewire.docente-asignaturas.form')

				@if($docenteAsignaturas->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto') }}</th>
										<th scope="col">{{ __('Nombre completo') }}</th>
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Asignatura') }}</th>
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Aula') }}</th>
										<th scope="col">{{ __('Turno') }}</th>
										<th scope="col">{{ __('Día') }}</th>
										<th scope="col">{{ __('Hora de entrada') }}</th>
										<th scope="col">{{ __('Hora de salida') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($docenteAsignaturas as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->docente->user->foto_perfil) }}" alt="{{ $row->docente->user->username }}" width="110" height="110"></td>
										<td scope="row">{{$row->docente->user->apellido_paterno}} {{ __(' ') }} {{$row->docente->user->apellido_materno}} {{ __(' ') }} {{ $row->docente->user->nombre }}</td>
										<td scope="row">{{$row->docente->carrera->grado->abreviatura}} {{ __('en') }} {{ $row->docente->carrera->carrera }}</td>
										<td scope="row">{{ $row->grupo->cuatrimestre->cuatrimestre }}</td>
										<td scope="row">{{ $row->asignatura->asignatura }}</td>
										<td scope="row">{{ $row->grupo->grupo }}</td>
										<td scope="row">{{ $row->grupo->aula->aula }}</td>
										<td scope="row">{{ $row->grupo->turno->turno }}</td>
										<td scope="row">{{ $row->dia->dia }}</td>
										<td scope="row">{{ $row->hora_entrada }}</td>
										<td scope="row">{{ $row->hora_salida }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar la asignatura {{ $row->asignatura->asignatura }} del docente {{$row->docente->user->apellido_paterno}} {{ __(' ') }} {{$row->docente->user->apellido_materno}} {{ __(' ') }} {{ $row->docente->user->nombre }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar la asignatura {{ $row->asignatura->asignatura }} del docente {{$row->docente->user->apellido_paterno}} {{ __(' ') }} {{$row->docente->user->apellido_materno}} {{ __(' ') }} {{ $row->docente->user->nombre }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $docenteAsignaturas->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
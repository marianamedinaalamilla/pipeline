@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="selectDocente" class="form-select bg-light text-dark border-0 @error('selectDocente') is-invalid @enderror" required>
                @if ($docentes->count())
                    <option selected>{{ __('Selecciona un docente') }}</option>
                    @foreach($docentes as $item)
                        <option value="{{ $item->id }}">{{ $item->user->apellido_paterno }} {{ __(' ') }} {{ $item->user->apellido_materno }} {{ __(' ') }} {{ $item->user->nombre }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun docente') }}</option>
                @endif
            </select>
            <label for="selectDocente" class="text-dark">{{ __('Docentes') }}</label>
            @error('selectDocente')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if (!is_null($cuatrimestres2))
            <div class="form-floating mb-4">
                <select wire:model.lazy="selectCuatrimestre" class="form-select bg-light text-dark border-0 @error('selectCuatrimestre') is-invalid @enderror" required>
                @if ($cuatrimestres2->count())
                    <option selected>{{ __('Selecciona un cuatrimestre') }}</option>
                    @foreach($cuatrimestres2 as $item)
                        <option value="{{ $item->cuatrimestre_id }}">{{ $item->cuatrimestre->cuatrimestre }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun cuatrimestre') }}</option>
                @endif
                </select>
                <label for="selectCuatrimestre" class="text-dark">{{ __('Cuatrimestres') }}</label>
                @error('selectCuatrimestre')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
        @if (!is_null($asignaturas2))
            <div class="form-floating mb-4">
                <select wire:model.lazy="asignatura_id" class="form-select bg-light text-dark border-0 @error('asignatura_id') is-invalid @enderror" required>
                @if ($asignaturas2->count())
                    <option selected>{{ __('Selecciona una asignatura') }}</option>
                    @foreach($asignaturas2 as $item)
                        <option value="{{ $item->id }}">{{ $item->asignatura }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ninguna asignatura') }}</option>
                @endif
                </select>
                <label for="asignatura_id" class="text-dark">{{ __('Asignaturas') }}</label>
                @error('asignatura_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
        @if (!is_null($grupos2))
            <div class="form-floating mb-4">
                <select wire:model.lazy="grupo_id" class="form-select bg-light text-dark border-0 @error('grupo_id') is-invalid @enderror" required>
                @if ($grupos2->count())
                    <option selected>{{ __('Selecciona un grupo') }}</option>
                    @foreach($grupos2 as $item)
                        <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun grupo') }}</option>
                @endif
                </select>
                <label for="grupo_id" class="text-dark">{{ __('Grupos') }}</label>
                @error('grupo_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
        @if (!is_null($cuatrimestres2) && !is_null($asignaturas2) && !is_null($grupos2))
            <div class="form-floating mb-4">
                <select wire:model.lazy="dia_id" class="form-select bg-light text-dark border-0 @error('dia_id') is-invalid @enderror" required>
                @if ($dias->count())
                    <option selected>{{ __('Selecciona un día') }}</option>
                    @foreach($dias as $item)
                        <option value="{{ $item->id }}">{{ $item->dia }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun día') }}</option>
                @endif
                </select>
                <label for="grupo_id" class="text-dark">{{ __('Días') }}</label>
                @error('dia_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
            <div class="form-floating mb-4">
                <input wire:model.lazy="hora_entrada" type="time" class="form-control bg-light text-dark border-0 @error('hora_entrada') is-invalid @enderror" required>
                <label for="hora_entrada" class="text-dark">{{ __('Hora de entrada') }}</label>
                @error('hora_entrada')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
            <div class="form-floating mb-4">
                <input wire:model.lazy="hora_salida" type="time" class="form-control bg-light text-dark border-0 @error('hora_salida') is-invalid @enderror" required>
                <label for="hora_salida" class="text-dark">{{ __('Hora de salida') }}</label>
                @error('hora_salida')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
    </div>
</form>
@include('modal.footer')
@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <input wire:model.lazy="aula" type="number" class="form-control bg-light text-dark border-0 @error('aula') is-invalid @enderror" min="1" max="15" step="1" placeholder="Ingresa una aula" required>
            <label for="aula" class="text-dark">{{ __('Aula') }}</label>
            @error('aula')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="departamento" type="text" class="form-control bg-light text-dark border-0 @error('departamento') is-invalid @enderror" placeholder="Ingresa un departamento" required autocomplete="departamento">
            <label for="departamento" class="text-dark">{{ __('Departamento') }}</label>
            @error('departamento')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
</form>
@include('modal.footer')
@section('title', __('Cuatrimestres'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="icon">
									<img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/cuatrimestres.png') }}" alt="sensores">
								</i>
								{{ __('Cuatrimestres') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar cuatrimestre">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($cuatrimestres2->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCuatrimestre" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un cuatrimestre') }}</option>
										@foreach($cuatrimestres2 as $item)
											<option value="{{ $item->cuatrimestre }}">{{ $item->cuatrimestre }}</option>
										@endforeach
									</select>
									<label for="filtroCuatrimestre">{{ __('Cuatrimestres') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='filtroPeriodo' class="form-control bg-light text-dark border-0" type="date">
								<label for="filtroPeriodo">{{ __('Buscar un periodo cuatrimestral') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.cuatrimestres.form')

				@if($cuatrimestres->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Periodo cuatrimestral inicial') }}</th>
										<th scope="col">{{ __('Periodo cuatrimestral final') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($cuatrimestres as $row)
									<tr>
										<td scope="row">{{ $row->cuatrimestre }}</td>
										<td scope="row">{{ $row->periodo_cuatrimestral_inicial }}</td>
										<td scope="row">{{ $row->periodo_cuatrimestral_final }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el cuatrimestre {{ $row->cuatrimestre }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el cuatrimestre {{ $row->cuatrimestre }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $cuatrimestres->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <input wire:model.lazy="cuatrimestre" type="number" class="form-control cuatrimestre bg-light text-dark border-0 @error('cuatrimestre') is-invalid @enderror" min="1" max="11" step="1" placeholder="Ingresa un cuatrimestre" required>
            <label for="cuatrimestre" class="text-dark">{{ __('Cuatrimestre') }}</label>
            @error('cuatrimestre')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="pci" type="date" class="form-control pci bg-light text-dark border-0 @error('pci') is-invalid @enderror" required>
            <label for="pci" class="text-dark">{{ __('Periodo cuatrimestral inicial') }}</label>
            @error('pci')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="pcf" type="date" class="form-control pcf bg-light text-dark border-0 @error('pcf') is-invalid @enderror" required>
            <label for="pcf" class="text-dark">{{ __('Periodo cuatrimestral inicial') }}</label>
            @error('pcf')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
</form>
@include('modal.footer')
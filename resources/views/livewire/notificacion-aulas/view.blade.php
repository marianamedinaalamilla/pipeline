@section('title', __('Notificación de aulas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bi bi-bell icon"></i>
								{{ __('Notificación de aulas') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar notificación de aulas">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($grupos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGrupo" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un grupo') }}</option>
										@foreach($grupos as $item)
											<option value="{{ $item->id }}">{{ $item->grupo }}</option>
										@endforeach
									</select>
									<label for="filtroRoles">{{ __('Grupos') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='filtroFecha' class="form-control bg-light text-dark border-0" type="date">
								<label for="filtroFecha">{{ __('Buscar fecha') }}</label>
							</div>  
							<div class="form-floating mb-2">
								<input wire:model='filtroHora' class="form-control bg-light text-dark border-0" type="time">
								<label for="filtroHora">{{ __('Buscar hora') }}</label>
							</div>  
						</div>
					</div>
				</div>

				@include('livewire.notificacion-aulas.form')

				@if($notificacionAulas->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Aula') }}</th>
										<th scope="col">{{ __('Turno') }}</th>
										<th scope="col">{{ __('Fecha') }}</th>
										<th scope="col">{{ __('Hora para abrir') }}</th>
										<th scope="col">{{ __('Hora para cerrar') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($notificacionAulas as $row)
									<tr>
										<td scope="row">{{ $row->grupo->grupo }}</td>
										<td scope="row">{{ $row->grupo->carrera->carrera }}</td>
										<td scope="row">{{ $row->grupo->cuatrimestre->cuatrimestre }}</td>
										<td scope="row">{{ $row->grupo->aula->aula }}</td>
										<td scope="row">{{ $row->grupo->turno->turno }}</td>
										<td scope="row">{{ $row->fecha }}</td>
										<td scope="row">{{ $row->hora_abrir }}</td>
										<td scope="row">{{ $row->hora_cerrar }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar a la carrera de {{ $row->carrera }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar a la carrera de {{ $row->carrera }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $notificacionAulas->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
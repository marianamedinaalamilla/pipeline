@section('title', __('Estudiantes/Grupos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col mb-2">
							<a style="text-decoration: none; color: black;" href="{{ url('/estudiantes') }}">{{ __('Estudiantes') }}</a>{{ __('/') }}<b>{{ __('Grupos') }}</b>
						</div>
					</div>
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-group icon"></i>
								{{ __('Grupos') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar grupo para los estudiantes">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($estudiantes->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroEstudiante" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un estudiante') }}</option>
										@foreach($estudiantes as $item)
											<option value="{{ $item->id }}">{{ $item->user->apellido_paterno }} {{ __(' ') }} { {$item->user->apellido_materno }} {{ __(' ') }} {{ $item->user->nombre }}</option>
										@endforeach
									</select>
									<label for="filtroEstudiante">{{ __('Estudiantes') }}</label>
								</div>
							@endif
							@if($grupos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGrupo" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un grupo') }}</option>
										@foreach($grupos as $item)
											<option value="{{ $item->id }}">{{ $item->grupo }}</option>
										@endforeach
									</select>
									<label for="filtroGrupo">{{ __('Grupos') }}</label>
								</div>
							@endif
						</div>
					</div>
				</div>

				@include('livewire.estudiante-grupos.form')

				@if($estudianteGrupos->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto') }}</th>
										<th scope="col">{{ __('Nombre completo') }}</th>
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Aula') }}</th>
										<th scope="col">{{ __('Turno') }}</th>
										<th scope="col">{{ __('Asignaturas') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($estudianteGrupos as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->estudiante->user->foto_perfil) }}" alt="{{ $row->estudiante->user->username }}" width="110" height="110"></td>
										<td scope="row">{{$row->estudiante->user->apellido_paterno}} {{ __(' ') }} {{$row->estudiante->user->apellido_materno}} {{ __(' ') }} {{ $row->estudiante->user->nombre }}</td>
										<td scope="row">{{$row->estudiante->carrera->grado->abreviatura}} {{ __('en') }} {{ $row->estudiante->carrera->carrera }}</td>
										<td scope="row">{{ $row->grupo->cuatrimestre->cuatrimestre }}</td>
										<td scope="row">{{ $row->grupo->grupo }}</td>
										<td scope="row">{{ $row->grupo->aula->aula }}</td>
										<td scope="row">{{ $row->grupo->turno->turno }}</td>
										<td scope="row" class="text-center"></td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el grupo {{ $row->grupo->grupo }} del estudiante {{$row->estudiante->user->apellido_paterno}} {{ __(' ') }} {{$row->estudiante->user->apellido_materno}} {{ __(' ') }} {{ $row->estudiante->user->nombre }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el grupo {{ $row->grupo->grupo }} del estudiante {{$row->estudiante->user->apellido_paterno}} {{ __(' ') }} {{$row->estudiante->user->apellido_materno}} {{ __(' ') }} {{ $row->estudiante->user->nombre }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $estudianteGrupos->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
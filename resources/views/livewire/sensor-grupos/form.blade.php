@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="sensor_id" class="form-select bg-light text-dark border-0 @error('sensor_id') is-invalid @enderror" required>
                @if($sensores->count())
                    <option selected>{{ __('Selecciona un sensor') }}</option>
                    @foreach($sensores as $item)
                        <option value="{{ $item->id }}">{{ $item->modelo }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun sensor') }}</option>
                @endif
            </select>
            <label for="sensor_id" class="text-dark">{{ __('Sensores') }}</label>
            @error('sensor_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if ($identificador < 1)
            <div class="mb-4">
                <label for="grupo_id" class="text-dark">{{ __('Grupos') }}</label>
                <select wire:model.lazy="grupo_id" multiple class="form-select bg-light text-dark border-0 @error('grupo_id') is-invalid @enderror" required>
                    @if ($grupos->count())
                        @foreach($grupos as $item)
                            <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ningun grupo') }}</option>
                    @endif
                </select>
                @error('grupo_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @else
            <div class="form-floating mb-4">
                <select wire:model.lazy="grupo_id" class="form-select bg-light text-dark border-0 @error('grupo_id') is-invalid @enderror" required>
                    @if ($grupos->count())
                        <option selected>{{ __('Selecciona un grupo') }}</option>
                        @foreach($grupos as $item)
                            <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ningun grupo') }}</option>
                    @endif
                </select>
                <label for="grupo_id" class="text-dark">{{ __('Grupos') }}</label>
                @error('grupo_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
        @if ($identificador > 0)
            <div class="form-floating mb-4">
                <select wire:model.lazy="estatu_sensor_id" class="form-select bg-light text-dark border-0 @error('estatu_sensor_id') is-invalid @enderror" required>
                    <option selected>{{ __('Selecciona un estatu') }}</option>
                    @foreach($modos as $item)
                        <option value="{{$item->id}}">{{$item->estatu}}</option>
                    @endforeach
                </select>
                <label for="estatu_sensor_id" class="text-dark">{{ __('Estatus') }}</label>
                @error('estatu_sensor_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
    </div>
</form>
@include('modal.footer')
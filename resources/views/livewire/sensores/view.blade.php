@section('title', __('Sensores'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="icon">
									<img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/rfid.png') }}" alt="sensores">
								</i>
								{{ __('Sensores') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar sensor">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar sensor">
								<label for="buscador">{{ __('Buscar sensor') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.sensores.form')

				@if($sensores->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto') }}</th>
										<th scope="col">{{ __('Modelo') }}</th>
										<th scope="col">{{ __('Descripción') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($sensores as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->foto) }}" alt="{{ $row->modelo }}" width="110" height="110"></td>
										<td scope="row">{{ $row->modelo }}</td>
										<td scope="row">{{ $row->descripcion }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el sensor {{ $row->modelo }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el sensor {{ $row->modelo }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $sensores->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@extends('layouts.menu')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @livewire('sensores')
        </div>     
    </div>   
</div>
@endsection
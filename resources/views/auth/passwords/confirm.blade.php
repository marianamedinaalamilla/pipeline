@extends('layouts.sesion')

<!-- Title -->
@section('title')	
<title>{{ ('Confirmar contraseña') }}</title>
@endsection

@section('content')
<span class="padding-bottom--15">{{ __('Confirmar contraseña') }}</span>
<p>{{ __('Por favor, confirme su contraseña antes de continuar.') }}</p>
<form id="stripe-login" method="post" action="{{ route('password.confirm') }}" enctype="multipart/form-data">
	@csrf
	<div class="field padding-bottom--24">
		<label for="password">{{ __('Contraseña') }}</label>
		<input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="*****************" required autocomplete="current-password" autofocus>
		@error('password')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
    <div class="field padding-bottom--24">
		<div class="grid--50-50">
            @if (Route::has('password.request'))
			<label></label>
			<div class="reset-pass">
				<a href="{{ route('password.request') }}">{{ __('¿Olvidaste tu contraseña?') }}</a>
			</div>
            @endif
		</div>
	</div>
	<div class="field padding-bottom--24">
		<input type="submit" name="enviar" value="Confirmar contraseña">
	</div>
</form>

@section('js')	
@if(Session('status'))
<script type="text/javascript">
    Swal.fire(
        '¡Advertencia!',
        '{{ session("status") }}',
        'question'
    );
</script>
@endif
@endsection

@endsection
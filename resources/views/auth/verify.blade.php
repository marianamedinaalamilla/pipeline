@extends('layouts.menu')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <span class="h2 text-dark float-left fw-bold">
                                        <i class="bx bx-envelope icon"></i>
                                        {{ __('Verifique su dirección de correo electrónico') }}
                                    </span>
                                </div>
                            </div>
            
                            <div class="card-body">
                                @section('js')	
                                @if (session('resent'))
                                    <script type="text/javascript">
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.',
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                    </script>
                                @endif
                                @endsection
                            {{ __('Antes de continuar, verifique su correo electrónico para obtener un enlace de verificación.') }}
                            {{ __('Si no recibiste el correo electrónico') }},
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('haga clic aquí para solicitar otro') }}</button>.
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>   
@endsection

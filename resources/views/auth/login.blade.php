@extends('layouts.sesion')

<!-- Title -->
@section('title')	
<title>{{ ('Iniciar Sesión') }}</title>
@endsection

@section('content')
<span class="padding-bottom--15">{{ __('Iniciar Sesión') }}</span>
<form id="stripe-login" method="post" action="{{ route('login') }}" enctype="multipart/form-data">
	@csrf
	<div class="field padding-bottom--24">
		<label for="username">{{ __('Matricula') }}</label>
		<input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror" placeholder="Ingresa una matricula" value="{{ old('username') }}" required autocomplete="username" autofocus>
		@error('username')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
	<div class="field padding-bottom--24">
		<div class="grid--50-50">
			<label for="password">{{ __('Contraseña') }}</label>
			<div class="reset-pass">
				<a href="{{ route('password.request') }}">{{ __('¿Olvidaste tu contraseña?') }}</a>
			</div>
		</div>
		<input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="*****************" required autocomplete="current-password">
		@error('password')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
	<div class="field field-checkbox padding-bottom--24 flex-flex align-center">
		<label class="form-check-label" for="remember">
			<input type="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Recordar sesión') }}
		</label>
	</div>
	<div class="field padding-bottom--24">
		<input type="submit" name="enviar" value="Iniciar Sesión">
	</div>
</form>
@endsection
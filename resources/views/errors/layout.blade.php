<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<!-- Mobile Specific Metas -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="keywords" content="HTML, CSS, JavaScript, PHP">
        <meta name="author" content="Shock the System">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
		<title>@yield('title')</title>
		
        <!-- Favicons -->
		<link href="{{ asset('img/logos/512x512_claro_uteams.png') }}" rel="apple-touch-startup-image">  

		<!-- Vendor CSS Files -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
		@laravelPWA
        
		<!-- Template UTeams CSS File -->
		<link href="{{ asset('css/sesion.css') }}" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		<div class="container">
			<div class="login-root">
				<div class="box-root flex-flex flex-direction--column" style="min-height: 100vh;flex-grow: 1;">
					<div class="loginbackground box-background--white padding-top--64">
						<div class="loginbackground-gridContainer">
							<div class="box-root flex-flex" style="grid-area: top / start / 8 / end;">
								<div class="box-root" style="background-image: linear-gradient(white 0%, rgb(247, 250, 252) 33%); flex-grow: 1;">
								</div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 4 / 2 / auto / 5;">
								<div class="box-root box-divider--light-all-2 animationLeftRight tans3s" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 6 / start / auto / 2;">
								<div class="box-root box-background--white" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 7 / start / auto / 4;">
								<div class="box-root box-background--white animationLeftRight" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 8 / 4 / auto / 6;">
								<div class="box-root box-background--blue animationLeftRight tans3s" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 2 / 15 / auto / end;">
								<div class="box-root box-background--blue animationRightLeft tans4s" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 3 / 14 / auto / end;">
								<div class="box-root box-background--white animationRightLeft" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 4 / 17 / auto / 20;">
								<div class="box-root box-background--blue animationRightLeft tans4s" style="flex-grow: 1;"></div>
							</div>
							<div class="box-root flex-flex" style="grid-area: 5 / 14 / auto / 17;">
								<div class="box-root box-divider--light-all-2 animationRightLeft tans3s" style="flex-grow: 1;"></div>
							</div>
						</div>
					</div>
					<div class="box-root padding-top--24 flex-flex flex-direction--column" style="flex-grow: 1; z-index: 9;">
						<div class="box-root padding-top--48 padding-bottom--24 flex-flex flex-justifyContent--center">
							<img src="{{ asset('img/logos/171x82_claro_uteams.png') }}" alt="UTeams">
						</div>
						<div class="formbg-outer">
							<div class="formbg">
								<div class="formbg-inner padding-horizontal--48">
                                    <div class="mb-4 text-center">
										<h1>@yield('code', __('Oh no'))</h1>
									</div>
									<div class="mb-4">
										<h4>@yield('message')</h4>
									</div>
                                    <a href="@auth {{ route('home') }} @else {{ url('/') }} @endauth" class="field padding-bottom--24">
                                        <input type="submit" name="recargar" value="Recargar">
                                    </a>
								</div>
							</div>
							<div class="footer-link padding-top--24">
								<div class="listing padding-top--24 padding-bottom--24 flex-flex center-center">
									<span><a href="{{ url('/') }}">{{ __('©Shock The System') }}</a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	<!-- Vendor JS Files -->
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	
	<!-- Template UTeams JS File -->
    <script type="text/javascript" src="{{ asset('js/offline.js') }}"></script>
</html>
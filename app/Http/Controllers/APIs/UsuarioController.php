<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UsuarioController extends Controller
{

    public function login(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'username' => 'required',
			'password' => 'required'
        ]);

        if($validacion->fails()){
            return response()->json(['errores' => $validacion->errors()], 200);
        };

        $credencial = $request->only('username', 'password');

        if(!Auth::attempt($credencial)){
            return response()->json([ 'mensaje' => 'Estas credenciales no coinciden con nuestros registros.' ], 200);
        };
        
        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response()->json([
            'usuario' => Auth::user()->load(['genero', 'estatuUsuario', 'roles', 'docentes', 'estudiantes']),
            'token_acceso' => $accessToken
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([ 'mensaje' => 'Has cerrado la sesión con éxito' ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = User::with('genero', 'estatuUsuario', 'roles', 'docentes', 'estudiantes')->orderBy('apellido_paterno', 'ASC')->get();
        return response()->json([ 'usuarios' => $usuario ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = Usuario::with('genero', 'estatuUsuario', 'roles', 'docentes', 'estudiantes')->find($id); 
		return response()->json([ 'usuario' => $usuario ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

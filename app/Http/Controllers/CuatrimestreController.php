<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuatrimestre;

class CuatrimestreController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cuatrimestre' => 'required|integer|min:1|max:11',
            'pci' => 'required|date|before:pcf',
            'pcf' => 'required|date|after:pci'
        ]);

        $cuatrimestre = new Cuatrimestre();
        $cuatrimestre->cuatrimestre = $request->cuatrimestre;
        $cuatrimestre->periodo_cuatrimestral_inicial = $request->pci;
        $cuatrimestre->periodo_cuatrimestral_final = $request->pcf;
        return $cuatrimestre->save();
    }
}

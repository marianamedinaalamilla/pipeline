<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\GrupoAsignatura;
use App\Models\Grupo;
use App\Models\Asignatura;
use App\Models\Dia;

class GrupoAsignaturas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroGrupo, $filtroAsignatura, $filtroDia, $filtroHora, $tituloModulo, $selectGrupo, $asignatura_id, $dia_id, $hora_entrada, $hora_salida, $asignaturas2, $boton = false;

    public function render()
    {
		$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')->paginate(5);
        if($this->filtroGrupo != ""){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')->where('grupo_id', $this->filtroGrupo)->paginate(5);
        } else {
            $this->filtroGrupo = null;
        };
        if($this->filtroAsignatura != ""){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')->where('asignatura_id', $this->filtroAsignatura)->paginate(5);
        } else {
            $this->filtroAsignatura = null;
        };
        if($this->filtroDia != ""){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')->where('dia_id', $this->filtroDia)->paginate(5);
        } else {
            $this->filtroDia = null;
        };
        if($this->filtroHora != ""){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->orWhere('hora_entrada', $this->filtroHora)
                                                    ->orWhere('hora_salida', $this->filtroHora)
                                                    ->paginate(5);
        } else {
            $this->filtroHora = null;
        };
        if($this->filtroGrupo && $this->filtroAsignatura){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroDia){
			$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->paginate(5);
		} elseif ($this->filtroGrupo && $this->filtroHora){
			$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
		} elseif ($this->filtroAsignatura && $this->filtroDia){
			$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->paginate(5);
		} elseif ($this->filtroAsignatura && $this->filtroHora){
			$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
		} elseif ($this->filtroDia && $this->filtroHora){
			$grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
		};
        if($this->filtroGrupo && $this->filtroAsignatura && $this->filtroDia){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroAsignatura && $this->filtroHora){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroDia && $this->filtroHora){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
        };
        if($this->filtroGrupo && $this->filtroAsignatura && $this->filtroDia && $this->filtroHora){
            $grupoAsignaturas = GrupoAsignatura::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('asignatura_id', $this->filtroAsignatura)
                                                    ->where('dia_id', $this->filtroDia)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_entrada', $this->filtroHora)
                                                            ->orWhere('hora_salida', $this->filtroHora);
                                                        })
                                                    ->paginate(5);
        };
        $grupos = Grupo::orderBy('grupo', 'ASC')->get();
        $asignaturas = Asignatura::orderBy('asignatura', 'ASC')->get();
        $dias = Dia::all();
        return view('livewire.grupo-asignaturas.view', compact('grupoAsignaturas', 'grupos', 'asignaturas', 'dias'));
    }

    public function mount(){
		$this->tituloModulo = 'Asignatura';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->selectGrupo = null;
        $this->asignatura_id = null;
        $this->dia_id = null;
        $this->hora_entrada = null;
		$this->hora_salida = null;
        $this->asignaturas2 = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
            'selectGrupo' => 'required|integer',
            'asignatura_id' => 'required|integer',
            'dia_id' => 'required|array',
            'hora_entrada' => 'required|array',
			'hora_salida' => 'required|array'
        ]);

        foreach($this->dia_id as $dia){
            $grupoAsignatura = new GrupoAsignatura();
            $grupoAsignatura->grupo_id = $this->selectGrupo;
            $grupoAsignatura->asignatura_id = $this->asignatura_id;
            $grupoAsignatura->dia_id = $dia;
            $grupoAsignatura->hora_entrada = $this->hora_entrada[$dia];
            $grupoAsignatura->hora_salida = $this->hora_salida[$dia];
            $grupoAsignatura->save();
        };

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $grupoAsignatura = GrupoAsignatura::findOrFail($id);
        $grupo = Grupo::findOrFail($grupoAsignatura->grupo_id);

        $this->asignaturas2 = $grupo->carrera->asignaturas()->orderBy('asignatura')->where('cuatrimestre_id', $grupo->cuatrimestre_id)->get();
        $this->identificador = $id; 
		$this->selectGrupo = $grupoAsignatura->grupo_id;
        $this->asignatura_id = $grupoAsignatura->asignatura_id;
        $this->dia_id[$grupoAsignatura->dia_id] = $grupoAsignatura->dia_id;
        $this->hora_entrada[$grupoAsignatura->dia_id] = $grupoAsignatura->hora_entrada;
		$this->hora_salida[$grupoAsignatura->dia_id] = $grupoAsignatura->hora_salida;
    }

    public function update()
    {
        $this->validate([
            'selectGrupo' => 'required|integer',
            'asignatura_id' => 'required|integer',
            'dia_id' => 'required|array',
            'hora_entrada' => 'required|array',
			'hora_salida' => 'required|array'
        ]);

        if ($this->identificador) {
            foreach($this->dia_id as $dia){
                $grupoAsignatura = GrupoAsignatura::find($this->identificador);
                $grupoAsignatura->grupo_id = $this->selectGrupo;
                $grupoAsignatura->asignatura_id = $this->asignatura_id;
                $grupoAsignatura->dia_id = $dia;
                $grupoAsignatura->hora_entrada = $this->hora_entrada[$dia];
                $grupoAsignatura->hora_salida = $this->hora_salida[$dia];
                $grupoAsignatura->save();
            };

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $grupoAsignatura = GrupoAsignatura::find($id);
        $grupoAsignatura->delete();
        $this->emit('registroEliminado');
    }

    public function updatedselectGrupo($id)
    {
        $this->asignaturas2 = Grupo::find($id)->carrera->asignaturas()->orderBy('asignatura')->where('cuatrimestre_id', Grupo::find($id)->cuatrimestre_id)->get();
    }
}
<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Cuatrimestre;

class Cuatrimestres extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroCuatrimestre, $filtroPeriodo, $tituloModulo, $cuatrimestre, $pci, $pcf, $boton = false;

    public function render()
    {
		$cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->paginate(5);
        if($this->filtroCuatrimestre != ""){
            $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->where('cuatrimestre', $this->filtroCuatrimestre)->paginate(5);
        } else {
            $this->filtroCuatrimestre = null;
        };
        if($this->filtroPeriodo != ""){
            $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')
                                            ->orWhere('periodo_cuatrimestral_inicial', $this->filtroPeriodo)
                                            ->orWhere('periodo_cuatrimestral_final', $this->filtroPeriodo)
                                            ->paginate(5);
        } else {
            $this->filtroPeriodo = null;
        };
        if($this->filtroCuatrimestre && $this->filtroPeriodo){
            $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')
                                            ->where('cuatrimestre', $this->filtroCuatrimestre)
                                            ->where(function ($query) {
                                                $query->orWhere('periodo_cuatrimestral_inicial', $this->filtroPeriodo)
                                                    ->orWhere('periodo_cuatrimestral_final', $this->filtroPeriodo);
                                                })
                                            ->paginate(5);
        };
        $cuatrimestres2 = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->get();
        return view('livewire.cuatrimestres.view', compact('cuatrimestres', 'cuatrimestres2'));
    }

    public function mount(){
		$this->tituloModulo = 'Cuatrimestre';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
		$this->cuatrimestre = null;
        $this->pci = null;
        $this->pcf = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
            'cuatrimestre' => 'required|integer|min:1|max:11',
            'pci' => 'required|date|before:pcf',
            'pcf' => 'required|date|after:pci'
        ]);

        $cuatrimestre = new Cuatrimestre();
        $cuatrimestre->cuatrimestre = $this->cuatrimestre;
        $cuatrimestre->periodo_cuatrimestral_inicial = $this->pci;
        $cuatrimestre->periodo_cuatrimestral_final = $this->pcf;
		$cuatrimestre->save();
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $cuatrimestre = Cuatrimestre::findOrFail($id);

        $this->identificador = $id; 
		$this->cuatrimestre = $cuatrimestre->cuatrimestre;
        $this->pci = $cuatrimestre->periodo_cuatrimestral_inicial;
        $this->pcf = $cuatrimestre->periodo_cuatrimestral_final;		
    }

    public function update()
    {
        $this->validate([
            'cuatrimestre' => 'required|integer|min:1|max:11',
            'pci' => 'required|date|before:pcf',
            'pcf' => 'required|date|after:pci'
        ]);

        if ($this->identificador) {
			$cuatrimestre = Cuatrimestre::find($this->identificador);
            $cuatrimestre->cuatrimestre = $this->cuatrimestre;
            $cuatrimestre->periodo_cuatrimestral_inicial = $this->pci;
            $cuatrimestre->periodo_cuatrimestral_final = $this->pcf;
		    $cuatrimestre->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $cuatrimestre = Cuatrimestre::find($id);
        $cuatrimestre->delete();
        $this->emit('registroEliminado');
    }
}

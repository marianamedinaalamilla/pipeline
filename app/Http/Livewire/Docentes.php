<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Docente;
use App\Models\User;
use App\Models\Carrera;

class Docentes extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroUser, $filtroCarrera, $tituloModulo, $user_id, $carrera_id, $identificacion, $boton = false;

    public function render()
    {
        $docentes = Docente::orderBy('user_id', 'DESC')->paginate(5);
        if($this->filtroUser != ""){
            $this->buscador = null;
            $docentes = Docente::orderBy('user_id', 'DESC')->where('user_id', $this->filtroUser)->paginate(5);
        } else {
            $this->filtroUser = null;
        };
        if($this->filtroCarrera != ""){
            $this->buscador = null;
            $docentes = Docente::orderBy('user_id', 'DESC')->where('carrera_id', $this->filtroCarrera)->paginate(5);
        } else {
            $this->filtroCarrera = null;
        };
        if($this->filtroUser && $this->filtroCarrera){
            $docentes = Docente::orderBy('user_id', 'DESC')
                                    ->where('user_id', $this->filtroUser)
                                    ->where('carrera_id', $this->filtroCarrera)
                                    ->paginate(5);
        };
        if($this->buscador){
            $this->filtroUser = null;
            $this->filtroCarrera = null;
            $docentes = Docente::orderBy('user_id', 'DESC')
                                    ->where('identificacion', 'LIKE', '%'.$this->buscador.'%')
                                    ->paginate(5);
        };
        $users = User::orderBy('apellido_paterno', 'ASC')->get();
        $carreras = Carrera::orderBy('carrera', 'ASC')->get();
        return view('livewire.docentes.view', compact('docentes', 'users', 'carreras'));
    }

    public function mount(){
		$this->tituloModulo = 'Docente';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->user_id = null;
		$this->carrera_id = null;
        $this->identificacion = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'user_id' => 'required|integer',
            'carrera_id' => 'required|integer',
            'identificacion' => 'required|alpha_num|min:1|max:100|unique:docentes',
        ]);

        $docente = new Docente();
        $docente->user_id = $this->user_id;
		$docente->carrera_id = $this->carrera_id;
        $docente->identificacion = $this->identificacion;
		$docente->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $docente = Docente::findOrFail($id);

        $this->identificador = $id; 
		$this->user_id = $docente->user_id;
		$this->carrera_id = $docente->carrera_id;		
        $this->identificacion = $docente->identificacion;	
    }

    public function update()
    {
        $this->validate([
			'user_id' => 'required|integer',
            'carrera_id' => 'required|integer',
            'identificacion' => 'required|alpha_num|min:1|max:100|unique:docentes,identificacion,'.$this->identificador
        ]);

        if ($this->identificador) {
            $docente = Docente::find($this->identificador);
			$docente->user_id = $this->user_id;
            $docente->carrera_id = $this->carrera_id;
            $docente->identificacion = $this->identificacion;
            $docente->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $docente = Docente::find($id);
        $docente->delete();
        $this->emit('registroEliminado');
    }
}

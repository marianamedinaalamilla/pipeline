<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\SensorGrupo;
use App\Models\Sensor;
use App\Models\Grupo;
use App\Models\EstatuSensor;

class SensorGrupos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroSensor, $filtroGrupo, $filtroEstatus, $tituloModulo, $sensor_id, $grupo_id, $estatu_sensor_id, $boton = false;

    public function render()
    {
        $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')->paginate(5);
        if($this->filtroSensor != ""){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')->where('sensor_id', $this->filtroSensor)->paginate(5);
        } else {
            $this->filtroSensor = null;
        };
        if($this->filtroGrupo != ""){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')->where('grupo_id', $this->filtroGrupo)->paginate(5);
        } else {
            $this->filtroGrupo = null;
        };
        if($this->filtroEstatus != ""){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')->where('estatu_sensor_id', $this->filtroEstatus)->paginate(5);
        } else {
            $this->filtroEstatus = null;
        };
        if($this->filtroSensor && $this->filtroGrupo){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')
                                            ->where('sensor_id', $this->filtroSensor)
                                            ->where('grupo_id', $this->filtroGrupo)
                                            ->paginate(5);
        } elseif ($this->filtroSensor &&  $this->filtroEstatus){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')
                                            ->where('sensor_id', $this->filtroSensor)
                                            ->where('estatu_sensor_id', $this->filtroEstatus)
                                            ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroEstatus){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')
                                            ->where('grupo_id', $this->filtroGrupo)
                                            ->where('estatu_sensor_id', $this->filtroEstatus)
                                            ->paginate(5);
        };
        if($this->filtroSensor && $this->filtroGrupo && $this->filtroEstatus){
            $sensorGrupos = SensorGrupo::orderBy('sensor_id', 'ASC')
                                            ->where('sensor_id', $this->filtroSensor)
                                            ->where('grupo_id', $this->filtroGrupo)
                                            ->where('estatu_sensor_id', $this->filtroEstatus)
                                            ->paginate(5);
        };
        $sensores = Sensor::orderBy('modelo', 'ASC')->get();
        $grupos = Grupo::orderBy('grupo', 'ASC')->get();
        $modos = EstatuSensor::orderBy('estatu', 'ASC')->get();
        return view('livewire.sensor-grupos.view', compact('sensorGrupos', 'sensores', 'grupos', 'modos'));
    }

	public function mount(){
		$this->tituloModulo = 'Grupo';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {	
        $this->identificador = null;	
        $this->sensor_id = null;
		$this->grupo_id = null;
		$this->estatu_sensor_id = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'sensor_id' => 'required|integer',
            'grupo_id' => 'required|integer'
        ]);

		$sensorGrupo = new SensorGrupo();
        $sensorGrupo->sensor_id = $this->sensor_id;
		$sensorGrupo->grupo_id = $this->grupo_id;
		$sensorGrupo->estatu_sensor_id = EstatuSensor::where('estatu', 'Activo')->first()->id;
		$sensorGrupo->save();
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $sensorGrupo = SensorGrupo::findOrFail($id);

        $this->identificador = $id; 
		$this->sensor_id = $sensorGrupo->sensor_id;
		$this->grupo_id = $sensorGrupo->grupo_id;
		$this->estatu_sensor_id = $sensorGrupo->estatu_sensor_id;
    }

    public function update()
    {
        $this->validate([
			'sensor_id' => 'required|integer',
            'grupo_id' => 'required|integer',
            'estatu_sensor_id' => 'required|integer',
        ]);

        if ($this->identificador) {
			$sensorGrupo = SensorGrupo::find($this->identificador);
            $sensorGrupo->sensor_id = $this->sensor_id;
            $sensorGrupo->grupo_id = $this->grupo_id;
            $sensorGrupo->estatu_sensor_id = $this->estatu_sensor_id;
            $sensorGrupo->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        };
    }

    public function destroy($id)
    {
		$sensorGrupo = SensorGrupo::find($id);
        $sensorGrupo->delete();
        $this->emit('registroEliminado');
    }
}
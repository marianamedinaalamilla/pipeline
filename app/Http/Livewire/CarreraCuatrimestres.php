<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\CarreraCuatrimestre;
use App\Models\Carrera;
use App\Models\Cuatrimestre;

class CarreraCuatrimestres extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroCarrera, $filtroCuatrimestre, $tituloModulo, $carrera_id, $cuatrimestre_id, $boton = false;

    public function render()
    {
        $carreraCuatrimestres = CarreraCuatrimestre::orderBy('carrera_id', 'ASC')->paginate(5);
        if($this->filtroCarrera != ""){
            $carreraCuatrimestres = CarreraCuatrimestre::orderBy('carrera_id', 'ASC')->where('carrera_id', $this->filtroCarrera)->paginate(5);
        } else {
            $this->filtroCarrera = null;
        };
        if($this->filtroCuatrimestre != ""){
            $carreraCuatrimestres = CarreraCuatrimestre::orderBy('carrera_id', 'ASC')->where('cuatrimestre_id', $this->filtroCuatrimestre)->paginate(5);
        } else {
            $this->filtroCuatrimestre = null;
        };
        if($this->filtroCarrera && $this->filtroCuatrimestre){
            $carreraCuatrimestres = CarreraCuatrimestre::orderBy('carrera_id', 'ASC')
                                                            ->where('carrera_id', $this->filtroCarrera)
                                                            ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                            ->paginate(5);
        };
        $carreras = Carrera::orderBy('carrera', 'ASC')->get();
        $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->get();
        return view('livewire.carrera-cuatrimestres.view', compact('carreraCuatrimestres', 'carreras', 'cuatrimestres'));
    }

    public function mount(){
		$this->tituloModulo = 'Cuatrimestre';
	}

    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }

    private function resetInput()
    {		
        $this->identificador = null;
		$this->carrera_id = null;
		$this->cuatrimestre_id = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'carrera_id' => 'required|integer',
            'cuatrimestre_id' => 'required|array'
        ]);

        foreach($this->cuatrimestre_id as $item){
            $carreraCuatrimestre = new CarreraCuatrimestre();
            $carreraCuatrimestre->carrera_id = $this->carrera_id;
            $carreraCuatrimestre->cuatrimestre_id = $item;
            $carreraCuatrimestre->save();
        };

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $carreraCuatrimestre = CarreraCuatrimestre::findOrFail($id);

        $this->identificador = $id; 
		$this->carrera_id = $carreraCuatrimestre->carrera_id;
		$this->cuatrimestre_id = $carreraCuatrimestre->cuatrimestre_id;		
    }

    public function update()
    {
        $this->validate([
			'carrera_id' => 'required|integer',
            'cuatrimestre_id' => 'required|integer'
        ]);

        if ($this->identificador) {
			$carreraCuatrimestre = CarreraCuatrimestre::find($this->identificador);
            $carreraCuatrimestre->carrera_id = $this->carrera_id;
            $carreraCuatrimestre->cuatrimestre_id = $this->cuatrimestre_id;
            $carreraCuatrimestre->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $carreraCuatrimestre = CarreraCuatrimestre::find($id);
        $carreraCuatrimestre->delete();
        $this->emit('registroEliminado');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Grupo;
use App\Models\Carrera;
use App\Models\Cuatrimestre;
use App\Models\Aula;
use App\Models\Turno;

class Grupos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroCarrera, $filtroCuatrimestre, $filtroAula, $filtroTurno, $tituloModulo, $grupo, $selectCarrera, $cuatrimestre_id, $aula_id, $turno_id, $cuatrimestres2, $boton = false;

    public function render()
    {
		$grupos = Grupo::orderBy('grupo', 'ASC')->paginate(5);
        if($this->filtroCarrera != ""){
            $this->buscador = null;
            $grupos = Grupo::orderBy('grupo', 'ASC')->where('carrera_id', $this->filtroCarrera)->paginate(5);
        } else {
            $this->filtroCarrera = null;
        };
        if($this->filtroCuatrimestre != ""){
            $this->buscador = null;
            $grupos = Grupo::orderBy('grupo', 'ASC')->where('cuatrimestre_id', $this->filtroCuatrimestre)->paginate(5);
        } else {
            $this->filtroCuatrimestre = null;
        };
		if($this->filtroAula != ""){
            $this->buscador = null;
            $grupos = Grupo::orderBy('grupo', 'ASC')->where('aula_id', $this->filtroAula)->paginate(5);
        } else {
            $this->filtroAula = null;
        };
		if($this->filtroTurno != ""){
            $this->buscador = null;
            $grupos = Grupo::orderBy('grupo', 'ASC')->where('turno_id', $this->filtroTurno)->paginate(5);
        } else {
            $this->filtroTurno = null;
        };
        if($this->filtroCarrera && $this->filtroCuatrimestre){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('carrera_id', $this->filtroCarrera)
                                ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                            	->paginate(5);
        } elseif ($this->filtroCarrera && $this->filtroAula){
			$grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('carrera_id', $this->filtroCarrera)
                                ->where('aula_id', $this->filtroAula)
                                ->paginate(5);
		} elseif ($this->filtroCarrera && $this->filtroTurno){
			$grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('carrera_id', $this->filtroCarrera)
                                ->where('turno_id', $this->filtroTurno)
                                ->paginate(5);
		} elseif ($this->filtroCuatrimestre && $this->filtroAula){
			$grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                ->where('aula_id', $this->filtroAula)
                                ->paginate(5);
		} elseif ($this->filtroCuatrimestre && $this->filtroTurno){
			$grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                ->where('turno_id', $this->filtroTurno)
                                ->paginate(5);
		} elseif ($this->filtroAula && $this->filtroTurno){
			$grupos = Grupo::orderBy('grupo', 'ASC')
                                ->where('aula_id', $this->filtroAula)
                                ->where('turno_id', $this->filtroTurno)
                                ->paginate(5);
		};
		if($this->filtroCarrera && $this->filtroCuatrimestre && $this->filtroAula){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                            	->where('carrera_id', $this->filtroCarrera)
                            	->where('cuatrimestre_id', $this->filtroCuatrimestre)
                            	->where('aula_id', $this->filtroAula)
                            	->paginate(5);
        } elseif ($this->filtroCarrera && $this->filtroCuatrimestre && $this->filtroTurno){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                            	->where('carrera_id', $this->filtroCarrera)
                            	->where('cuatrimestre_id', $this->filtroCuatrimestre)
								->where('turno_id', $this->filtroTurno)
                            	->paginate(5);
        } elseif ($this->filtroCarrera && $this->filtroAula && $this->filtroTurno){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                            	->where('carrera_id', $this->filtroCarrera)
                            	->where('aula_id', $this->filtroAula)
								->where('turno_id', $this->filtroTurno)
                            	->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAula && $this->filtroTurno){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                            	->where('cuatrimestre_id', $this->filtroCuatrimestre)
                            	->where('aula_id', $this->filtroAula)
								->where('turno_id', $this->filtroTurno)
                            	->paginate(5);
        };
		if($this->filtroCarrera && $this->filtroCuatrimestre && $this->filtroAula && $this->filtroTurno){
            $grupos = Grupo::orderBy('grupo', 'ASC')
                            	->where('carrera_id', $this->filtroCarrera)
                            	->where('cuatrimestre_id', $this->filtroCuatrimestre)
                            	->where('aula_id', $this->filtroAula)
								->where('turno_id', $this->filtroTurno)
                            	->paginate(5);
        };
        if($this->buscador){
            $this->filtroCarrera = null;
            $this->filtroCuatrimestre = null;
			$this->filtroAula = null;
            $this->filtroTurno = null;
            $grupos = Grupo::orderBy('grupo', 'ASC')->where('grupo', 'LIKE', '%'.$this->buscador.'%')->paginate(5);
        };
        $carreras = Carrera::orderBy('carrera', 'ASC')->get();
        $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->get();
		$aulas = Aula::orderBy('aula', 'ASC')->get();
		$turnos = Turno::orderBy('turno', 'ASC')->get();
        return view('livewire.grupos.view', compact('grupos', 'carreras', 'cuatrimestres', 'aulas', 'turnos'));
    }

    public function mount(){
		$this->tituloModulo = 'Grupo';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->grupo = null;
        $this->selectCarrera = null;
        $this->cuatrimestre_id = null;
		$this->aula_id = null;
		$this->turno_id = null;
        $this->cuatrimestres2 = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
            'grupo' => 'required|alpha_num|min:1|max:10|unique:grupos',
            'selectCarrera' => 'required|integer',
            'cuatrimestre_id' => 'required|integer',
			'aula_id' => 'required|integer',
			'turno_id' => 'required|integer'
        ]);

        $grupo = new Grupo();
        $grupo->grupo = $this->grupo;
        $grupo->carrera_id = $this->selectCarrera;
        $grupo->cuatrimestre_id = $this->cuatrimestre_id;
		$grupo->aula_id = $this->aula_id;
		$grupo->turno_id = $this->turno_id;
		$grupo->save();
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $grupo = Grupo::findOrFail($id);
        $carrera = $grupo->carrera_id;

        $this->cuatrimestres2 = Carrera::findOrFail($carrera)->carreraCuatrimestres()->orderBy('carrera_id')->get();
        $this->identificador = $id; 
		$this->grupo = $grupo->grupo;
        $this->selectCarrera = $carrera ;
        $this->cuatrimestre_id = $grupo->cuatrimestre_id;
		$this->aula_id = $grupo->aula_id;
		$this->turno_id = $grupo->turno_id;
    }

    public function update()
    {
        $this->validate([
            'grupo' => 'required|alpha_num|min:1|max:10|unique:grupos,grupo,'.$this->identificador,
            'selectCarrera' => 'required|integer',
            'cuatrimestre_id' => 'required|integer',
			'aula_id' => 'required|integer',
			'turno_id' => 'required|integer'
        ]);

        if ($this->identificador) {
			$grupo = Grupo::find($this->identificador);
            $grupo->grupo = $this->grupo;
            $grupo->carrera_id = $this->selectCarrera;
            $grupo->cuatrimestre_id = $this->cuatrimestre_id;
			$grupo->aula_id = $this->aula_id;
			$grupo->turno_id = $this->turno_id;
		    $grupo->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $grupo = Grupo::find($id);
        $grupo->delete();
        $this->emit('registroEliminado');
    }

    public function updatedselectCarrera($id)
    {
        $this->cuatrimestres2 = Carrera::find($id)->carreraCuatrimestres()->orderBy('carrera_id')->get();
    }
}
<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\EstudianteGrupo;
use App\Models\Estudiante;
use App\Models\Grupo;

class EstudianteGrupos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroEstudiante, $filtroGrupo, $tituloModulo, $selectEstudiante, $grupo_id, $grupo2, $boton = false;

    public function render()
    {
        $estudianteGrupos = EstudianteGrupo::orderBy('estudiante_id', 'ASC')->paginate(5);
        if($this->filtroEstudiante != ""){
            $estudianteGrupos = EstudianteGrupo::orderBy('estudiante_id', 'ASC')->where('estudiante_id', $this->filtroEstudiante)->paginate(5);
        } else {
            $this->filtroEstudiante = null;
        };
        if($this->filtroGrupo != ""){
            $estudianteGrupos = EstudianteGrupo::orderBy('estudiante_id', 'ASC')->where('grupo_id', $this->filtroGrupo)->paginate(5);
        } else {
            $this->filtroGrupo = null;
        };
        if($this->filtroEstudiante && $this->filtroGrupo){
            $estudianteGrupos = EstudianteGrupo::orderBy('estudiante_id', 'ASC')
                                                    ->where('estudiante_id', $this->filtroEstudiante)
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->paginate(5);
        };
        $estudiantes = Estudiante::orderBy('user_id', 'DESC')->get();
        $grupos = Grupo::orderBy('grupo', 'ASC')->get();
        return view('livewire.estudiante-grupos.view', compact('estudianteGrupos', 'estudiantes', 'grupos'));
    }

    public function mount(){
		$this->tituloModulo = 'Grupo';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->selectEstudiante = null;
		$this->grupo_id = null;
        $this->grupo2 = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'selectEstudiante' => 'required|integer',
            'grupo_id' => 'required|integer'
        ]);

        $estudianteGrupo = new EstudianteGrupo();
        $estudianteGrupo->estudiante_id = $this->selectEstudiante;
		$estudianteGrupo->grupo_id = $this->grupo_id;
		$estudianteGrupo->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $estudianteGrupo = EstudianteGrupo::findOrFail($id);
        $estudiante = $estudianteGrupo->estudiante_id;

        $this->grupo2 = Estudiante::findOrFail($estudiante)->carrera->grupos()->orderBy('grupo')->get();
        $this->identificador = $id; 
		$this->selectEstudiante = $estudiante;
		$this->grupo_id = $estudianteGrupo->grupo_id;		
    }

    public function update()
    {
        $this->validate([
			'selectEstudiante' => 'required|integer',
            'grupo_id' => 'required|integer'
        ]);

        if ($this->identificador) {
			$estudianteGrupo = EstudianteGrupo::find($this->identificador);
            $estudianteGrupo->estudiante_id = $this->selectEstudiante;
		    $estudianteGrupo->grupo_id = $this->grupo_id;
		    $estudianteGrupo->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $estudianteGrupo = EstudianteGrupo::find($id);
        $estudianteGrupo->delete();
        $this->emit('registroEliminado');
    }

    public function updatedselectEstudiante($id)
    {
        $this->grupo2 = Estudiante::find($id)->carrera->grupos()->orderBy('grupo')->get();
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use App\Models\Genero;
use Spatie\Permission\Models\Role;
use App\Models\EstatuUsuario;
use App\Notifications\UserPush;
use App\Notifications\FotoPerfilPush;

class Users extends Component
{
    use WithPagination, WithFileUploads;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy', 'image'];
    public $identificador, $buscador, $filtroGenero, $filtroRoles, $filtroEstatus, $tituloModulo, $nombre, $ap, $am, $genero_id, $email, $username, $password, $fp, $rol, $estatu_usuario_id, $tmp, $boton = false;

    public function render()
    {
        $users = User::orderBy('apellido_paterno', 'ASC')->paginate(5);
        if($this->filtroGenero != ""){
            $this->buscador = null;
            $users = User::orderBy('apellido_paterno', 'ASC')->where('genero_id', $this->filtroGenero)->paginate(5);
        } else {
            $this->filtroGenero != null;
        };
        if($this->filtroRoles != ""){
            $buscador = null;
            $users = User::orderBy('apellido_paterno', 'ASC')->role($this->filtroRoles)->paginate(5);
        } else {
            $this->filtroRoles = null;
        };
        if($this->filtroEstatus != ""){
            $buscador = null;
            $users = User::orderBy('apellido_paterno', 'ASC')->where('estatu_usuario_id', $this->filtroEstatus)->paginate(5);
        } else {
            $this->filtroEstatus = null;
        };
        if($this->filtroGenero && $this->filtroRoles){
            $users = User::orderBy('apellido_paterno', 'ASC')
                            ->where('genero_id', $this->filtroGenero)
                            ->role($this->filtroRoles)
                            ->paginate(5);
        } elseif ($this->filtroGenero &&  $this->filtroEstatus){
            $users = User::orderBy('apellido_paterno', 'ASC')
                            ->where('genero_id', $this->filtroGenero)
                            ->where('estatu_usuario_id', $this->filtroEstatus)
                            ->paginate(5);
        } elseif ($this->filtroRoles && $this->filtroEstatus){
            $users = User::orderBy('apellido_paterno', 'ASC')
                            ->role($this->filtroRoles)
                            ->where('estatu_usuario_id', $this->filtroEstatus)
                            ->paginate(5);
        };
        if($this->filtroGenero && $this->filtroRoles && $this->filtroEstatus){
            $users = User::orderBy('apellido_paterno', 'ASC')
                            ->where('genero_id', $this->filtroGenero)
                            ->role($this->filtroRoles)
                            ->where('estatu_usuario_id', $this->filtroEstatus)
                            ->paginate(5);
        };
        if($this->buscador){
            $this->filtroGenero = null;
            $this->filtroRoles = null;
            $this->filtroEstatus = null;
            $users = User::orderBy('apellido_paterno', 'ASC')
                            ->orWhere('nombre', 'LIKE', '%'.$this->buscador.'%')
                            ->orWhere('apellido_paterno', 'LIKE', '%'.$this->buscador.'%')
                            ->orWhere('apellido_materno', 'LIKE', '%'.$this->buscador.'%')
                            ->orWhere('email', 'LIKE', '%'.$this->buscador.'%')
                            ->orWhere('username', 'LIKE', '%'.$this->buscador.'%')
                            ->paginate(5);
        };
        $generos = Genero::orderBy('genero', 'ASC')->get();
        $roles = Role::orderBy('name', 'ASC')->get();
        $modos = EstatuUsuario::orderBy('estatu', 'ASC')->get();
        return view('livewire.users.view', compact('users', 'generos', 'roles', 'modos'));
    }

	public function mount(){
		$this->tituloModulo = 'Usuario';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->tmp = null;
        $this->boton = false;
    }
	
    private function resetInput()
    {	
        $this->identificador = null;	
        $this->fp = null;
		$this->nombre = null;
		$this->ap = null;
		$this->am = null;
		$this->genero_id = null;
		$this->email = null;
		$this->username = null;
		$this->password = null;
		$this->rol = null;
        $this->estatu_usuario_id = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'nombre' => 'required|alpha_spaces|min:1|max:30',
			'ap' => 'required|alpha_spaces|min:1|max:15',
			'am' => 'required|alpha_spaces|min:1|max:15',
			'genero_id' => 'required|integer',
			'email' => 'required|email|min:1|max:31|unique:users',
			'username' => 'required|string|min:1|max:10|unique:users',
			'password' => 'required|alpha_num|min:8|max:20',
			'fp' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=110,min_height=110,max_width=180,max_height=180',
			'rol' => 'required|integer'
        ]);

		$usuario = new User();
        $usuario->nombre = $this->nombre;
		$usuario->apellido_paterno = $this->ap;
        $usuario->apellido_materno = $this->am;
        $usuario->genero_id = $this->genero_id;
        $usuario->email = $this->email;
        $usuario->username = $this->username;
        $usuario->password = bcrypt($this->password);
        $usuario->foto_perfil = 'storage/'.$this->fp->storeAs('img/perfil/'.$this->username, $this->username.'.'.$this->fp->extension());
        $usuario->estatu_usuario_id = EstatuUsuario::where('estatu', 'Activo')->first()->id;
		$usuario->save();
        $usuario->syncRoles($this->rol);
        event(new Registered($usuario));
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $usuario = User::findOrFail($id);

        $this->identificador = $id; 
		$this->nombre = $usuario->nombre;
		$this->ap = $usuario->apellido_paterno;
		$this->am = $usuario->apellido_materno;
		$this->genero_id = $usuario->genero_id;
		$this->email = $usuario->email;
		$this->username = $usuario->username;
		$this->fp = $usuario->foto_perfil;
        $this->tmp = $usuario->foto_perfil;
		foreach($usuario->roles as $item){
            if($item->guard_name == "web"){
                $this->rol = $item->id;
            };
        };
        $this->estatu_usuario_id = $usuario->estatu_usuario_id;
        $this->boton = true;
    }

    public function update()
    {
        $this->validate([
			'nombre' => 'required|alpha_spaces|min:1|max:30',
			'ap' => 'required|alpha_spaces|min:1|max:15',
			'am' => 'required|alpha_spaces|min:1|max:15',
			'genero_id' => 'required|integer',
			'email' => 'required|email|min:1|max:31|unique:users,email,'.$this->identificador,
			'username' => 'required|string|min:1|max:10|unique:users,username,'.$this->identificador,
			'rol' => 'required|integer'
        ]);

        if ($this->identificador) {
			$usuario = User::find($this->identificador);
            $usuario->nombre = $this->nombre;
            $usuario->apellido_paterno = $this->ap;
            $usuario->apellido_materno = $this->am;
            $usuario->genero_id = $this->genero_id;
            $usuario->email = $this->email;
            $usuario->username = $this->username;
            if(strlen($this->password) > 0){
                $this->validate(['password' => 'required|alpha_num|min:8|max:20']);
                $usuario->password = bcrypt($this->password);
            };
            $usuario->estatu_usuario_id = $this->estatu_usuario_id;
            foreach ($usuario->getRoleNames() as $item ){
                $usuario->removeRole($item);
            };
            $usuario->save();
            $usuario->syncRoles($this->rol);
            $usuario->notify(new UserPush);

            $this->resetInput();
            $this->boton = false;
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        };
    }

    public function destroy($id)
    {
		$usuario = User::find($id);
        $url = $usuario->foto_perfil;
        if(substr_count($url, 'storage/')){
            $url = str_replace('storage/', '', $url);
            if(Storage::disk('public')->exists($url)){
                Storage::disk('public')->delete($url);
                foreach ($usuario->getRoleNames() as $item ){
                    $usuario->removeRole($item);
                };
                $usuario->delete();
                $this->emit('registroEliminado');
            } else {
                foreach ($usuario->getRoleNames() as $item ){
                    $usuario->removeRole($item);
                };
                $usuario->delete();
                $this->emit('registroEliminado');
            };
        };

        if(File::exists(public_path($url))){
            $this->emit('registroError');
        };
    }

    public function image()
    {
        if ($this->identificador) {
            $usuario = User::find($this->identificador);
            if($this->fp){
                $this->validate(['fp' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=110,min_height=110,max_width=180,max_height=180']);
                Storage::disk('public')->delete($usuario->foto_perfil);
                $usuario->foto_perfil = 'storage/'.$this->fp->storeAs('img/perfil/'.$this->username, $this->username.'.'.$this->fp->extension());
                $usuario->save();
                $usuario->notify(new FotoPerfilPush);

                $this->resetInput();
                $this->tmp = null;
                $this->boton = false;
                $this->emit('modalCerrar');
                $this->emit('imagenActualizado');
            };
        };
    }
}
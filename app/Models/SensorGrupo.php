<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SensorGrupo extends Model
{
    public $timestamps = true;

    protected $table = 'sensor_grupos';

    protected $primaryKey = 'id';

    protected $fillable = ['sensor_id','grupo_id','estatu_sensor_id'];

    public function sensor(){
        return $this->belongsTo(Sensor::class);
    }

    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }

    public function estatuSensor(){
        return $this->belongsTo(EstatuSensor::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarreraCuatrimestre extends Model
{
    public $timestamps = true;

    protected $table = 'carrera_cuatrimestres';

    protected $primaryKey = 'id';

    protected $fillable = ['carrera_id','cuatrimestre_id'];

    public function carrera(){
        return $this->belongsTo(Carrera::class);
    }

    public function cuatrimestre(){
        return $this->belongsTo(Cuatrimestre::class);
    }
}

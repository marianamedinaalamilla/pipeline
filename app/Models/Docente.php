<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{	
    public $timestamps = true;

    protected $table = 'docentes';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id','carrera_id','identificacion'];
	
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function carrera(){
        return $this->belongsTo(Carrera::class);
    }
    
    public function docenteAsignatura(){
        return $this->hasMany(DocenteAsignatura::class);
    }
}

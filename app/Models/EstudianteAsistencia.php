<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudianteAsistencia extends Model
{	
  public $timestamps = true;
  
  protected $table = 'estudiante_asistencias';
  
  protected $primaryKey = 'id';
  
  protected $fillable = ['carrera_id', 'cuatrimestre_id','grupo_id','asignatura_id', 'estudiante_id', 'fecha', 'hora', 'estatu_asistencia_id'];

  public function carrera(){
    return $this->belongsTo(Carrera::class);
  }
  
  public function grupo(){
    return $this->belongsTo(Grupo::class);
  }
  
  public function asignatura(){
    return $this->belongsTo(Asignatura::class);
  }
  
  public function estudiante(){
    return $this->belongsTo(Estudiante::class);
  }
  
  public function estatuAsistencia(){
    return $this->belongsTo(EstatuAsistencia::class);
  }
}

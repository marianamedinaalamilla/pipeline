<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstatuSensor extends Model
{
    public $timestamps = true;

    protected $table = 'estatu_sensores';

    protected $primaryKey = 'id';
	
	protected $fillable = ['estatu'];
}

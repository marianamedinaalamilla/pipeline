<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dia extends Model
{
    public $timestamps = true;

    protected $table = 'dias';

    protected $primaryKey = 'id';

    protected $fillable = ['dia'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstatuUsuario extends Model
{
    public $timestamps = true;

    protected $table = 'estatu_usuarios';

    protected $primaryKey = 'id';
	
	protected $fillable = ['estatu'];
}

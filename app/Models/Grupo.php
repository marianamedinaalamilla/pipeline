<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{	
    public $timestamps = true;

    protected $table = 'grupos';

    protected $primaryKey = 'id';

    protected $fillable = ['grupo','carrera_id','cuatrimestre_id','aula_id','turno_id'];
	
    public function carrera(){
		return $this->belongsTo(Carrera::class);
	}

    public function cuatrimestre(){
		return $this->belongsTo(Cuatrimestre::class);
	}

    public function aula(){
		return $this->belongsTo(Aula::class);
	}

    public function turno(){
		return $this->belongsTo(Turno::class);
	}

	public function grupoAsignaturas(){
		return $this->hasMany(GrupoAsignatura::class);
	}

	public function docentes(){
		return $this->hasMany(Docente::class);
	}

	public function estudianteGrupos(){
		return $this->hasMany(EstudianteGrupo::class);
	}

	public function notificacionAulas(){
		return $this->hasMany(NotificacionAula::class);
	}

	public function sensorGrupos(){
		return $this->hasMany(SensorGrupo::class);
	}
}

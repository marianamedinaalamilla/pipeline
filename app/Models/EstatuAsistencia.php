<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstatuAsistencia extends Model
{
    public $timestamps = true;

    protected $table = 'estatu_asistencias';

    protected $primaryKey = 'id';
	
    protected $fillable = ['estatu'];
}

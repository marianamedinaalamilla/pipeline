<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocenteAsignatura extends Model
{	
    public $timestamps = true;

    protected $table = 'docente_asignaturas';

    protected $primaryKey = 'id';

    protected $fillable = ['docente_id','cuatrimestre_id','asignatura_id','grupo_id', 'dia_id', 'hora_entrada', 'hora_salida'];
	
    public function docente(){
        return $this->belongsTo(Docente::class);
    }
    
    public function asignatura(){
        return $this->belongsTo(Asignatura::class);
    }

    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }

    public function dia(){
        return $this->belongsTo(Dia::class);
    }
}

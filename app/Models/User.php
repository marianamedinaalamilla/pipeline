<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, HasRoles, HasPushSubscriptions;

    public $timestamps = true;

    protected $table = 'users';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['nombre','apellido_paterno','apellido_materno','genero_id','email', 'username', 'foto_perfil', 'estatu_usuario_id'];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['password','remember_token'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = ['email_verified_at' => 'datetime'];

    public function genero(){
        return $this->belongsTo(Genero::class);
    }
    
    public function estatuUsuario(){
        return $this->belongsTo(EstatuUsuario::class);
    }
    
    public function docentes(){
        return $this->hasMany(Docente::class);
    }
    
    public function estudiantes(){
        return $this->hasMany(Estudiante::class);
    }
}

<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    public $timestamps = true;

    protected $table = 'asignaturas';

    protected $primaryKey = 'id';

    protected $fillable = ['asignatura','carrera_id','cuatrimestre_id'];

    public function carrera(){
        return $this->belongsTo(Carrera::class);
	}

    public function cuatrimestre(){
        return $this->belongsTo(Cuatrimestre::class);
	}

    public function docentes(){
        return $this->hasMany(Docente::class);
    }

    public function grupoAsignaturas(){
        return $this->hasMany(GrupoAsignatura::class);
    }
}

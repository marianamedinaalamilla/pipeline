<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EstatuAsistencia;


class EstatuAsistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modo = new EstatuAsistencia();
		$modo->estatu = "Asistencia";
		$modo->save();

        $modo = new EstatuAsistencia();
		$modo->estatu = "Falta";
		$modo->save();

        $modo = new EstatuAsistencia();
		$modo->estatu = "Retardo";
		$modo->save();

        $modo = new EstatuAsistencia();
		$modo->estatu = "Justificado";
		$modo->save();
    }
}

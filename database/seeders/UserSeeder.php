<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use App\Models\Genero;
use Spatie\Permission\Models\Role;
use App\Models\EstatuUsuario;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$masculino = Genero::where('genero', 'Masculino')->first()->id;
		$femenino = Genero::where('genero', 'Femenino')->first()->id;
		$administradorWEB = Role::findByName("Administrador", "web");
		$estudianteWEB = Role::findByName("Estudiante", "web");
		$administradorAPI = Role::findByName("Administrador", "api");
		$estudianteAPI = Role::findByName("Estudiante", "api");
		$estatuUsuario = EstatuUsuario::where('estatu', 'Activo')->first()->id;

        $usuario = new User();
		$usuario->nombre = "Saúl Rodrigo";
		$usuario->apellido_paterno = "Rosado";
		$usuario->apellido_materno = "Cimé";
        $usuario->genero_id = $masculino;
		$usuario->email = "Saulrodrigorc@hotmail.com";
		$usuario->username = "SaulRC";
		$usuario->password = bcrypt("Saul1002");
		$usuario->foto_perfil = "img/administradores/$usuario->username/$usuario->username.jpg";
		$usuario->estatu_usuario_id = $estatuUsuario;
		$usuario->save();
        $usuario->syncRoles([$administradorWEB, $administradorAPI]);
		event(new Registered($usuario));
		
		$usuario = new User();
		$usuario->nombre = "Jesús Alberto";
		$usuario->apellido_paterno = "Gómez";
		$usuario->apellido_materno = "Hoil";
        $usuario->genero_id = $masculino;
		$usuario->email = "jesus20.tics@gmail.com";
		$usuario->username = "Jesus20";
		$usuario->password = bcrypt("Itkdm2017");
		$usuario->foto_perfil = "img/administradores/$usuario->username/$usuario->username.jpg";
		$usuario->estatu_usuario_id = $estatuUsuario;
		$usuario->save();
        $usuario->syncRoles([$estudianteWEB, $estudianteAPI]);
		//event(new Registered($usuario));
		
		$usuario = new User();
		$usuario->nombre = "Edgar David";
		$usuario->apellido_paterno = "Peech";
		$usuario->apellido_materno = "Chan";
        $usuario->genero_id = $masculino;
		$usuario->email = "deivid.pech095@gmail.com";
		$usuario->username = "Deivid95";
		$usuario->password = bcrypt("UTchetumal");
		$usuario->foto_perfil = "img/administradores/$usuario->username/$usuario->username.jpg";
		$usuario->estatu_usuario_id = $estatuUsuario;
		$usuario->save();
        $usuario->syncRoles([$estudianteWEB, $estudianteAPI]);
		//event(new Registered($usuario));
		
		$usuario = new User();
		$usuario->nombre = "Mariana Elizabeth";
		$usuario->apellido_paterno = "Medina";
		$usuario->apellido_materno = "Alamilla";
        $usuario->genero_id = $femenino;
		$usuario->email = "marianamedinaalamilla@gmail.com";
		$usuario->username = "Mariana00";
		$usuario->password = bcrypt("Eliza22m");
		$usuario->foto_perfil = "img/administradores/$usuario->username/$usuario->username.jpg";
		$usuario->estatu_usuario_id = $estatuUsuario;
		$usuario->save();
        $usuario->syncRoles([$estudianteWEB, $estudianteAPI]);
		//event(new Registered($usuario));
    }
}

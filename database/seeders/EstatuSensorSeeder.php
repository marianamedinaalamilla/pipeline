<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EstatuSensor;

class EstatuSensorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modo = new EstatuSensor();
		$modo->estatu = "Activo";
		$modo->save();
		
		$modo = new EstatuSensor();
		$modo->estatu = "Inactivo";
		$modo->save();
    }
}

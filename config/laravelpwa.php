<?php

return [
    'name' => 'UTeams',
    'manifest' => [
        'name' => env('APP_NAME', 'UTeams'),
        'short_name' => 'UTeams',
        'start_url' => '/',
        'description' => 'Una aplicación web para el control de asistencias de las universidades',
        'lang' => 'es',
        'background_color' => 'white',
        'theme_color' => '#303F9F',
        'display' => 'standalone',
        'orientation'=> 'any',
        'status_bar'=> '#303F9F',
        'icons' => [
            '48x48' => [
                'path' => '/img/logos/48x48_claro_uteams.png',
                'purpose' => 'any'
            ],
            '72x72' => [
                'path' => '/img/logos/72x72_claro_uteams.png',
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => '/img/logos/96x96_claro_uteams.png',
                'purpose' => 'any'
            ],
            '128x128' => [
                'path' => '/img/logos/128x128_claro_uteams.png',
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' => '/img/logos/144x144_claro_uteams.png',
                'purpose' => 'any'
            ],
            '152x152' => [
                'path' => '/img/logos/152x152_claro_uteams.png',
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => '/img/logos/192x192_claro_uteams.png',
                'purpose' => 'any'
            ],
            '384x384' => [
                'path' => '/img/logos/384x384_claro_uteams.png',
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => '/img/logos/512x512_claro_uteams.png',
                'purpose' => 'any'
            ],
        ],
        'splash' => [
            '640x1136' => '/img/splashs/640x1136.png',
            '750x1334' => '/img/splashs/750x1334.png',
            '828x1792' => '/img/splashs/828x1792.png',
            '1125x2436' => '/img/splashs/1125x2436.png',
            '1242x2208' => '/img/splashs/1242x2208.png',
            '1242x2688' => '/img/splashs/1242x2688.png',
            '1536x2048' => '/img/splashs/1536x2048.png',
            '1668x2224' => '/img/splashs/1668x2224.png',
            '1668x2388' => '/img/splashs/1668x2388.png',
            '2048x2732' => '/img/splashs/2048x2732.png',
        ],
        'shortcuts' => [],
        'custom' => []
    ]
];

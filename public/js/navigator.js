/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/navigator.js":
/*!***********************************!*\
  !*** ./resources/js/navigator.js ***!
  \***********************************/
/***/ (() => {

eval("(function () {\n  \"use strict\";\n\n  // Initialize The Service Worker.\n  if (navigator.serviceWorker) {\n    // Register a Service Worker hosted at the root of the site using the default scope.\n    navigator.serviceWorker.register('/serviceworker.js').then(function (registration) {\n      // Registration was successful.\n      console.log('El registro del Service Worker se realizó correctamente: ', registration.scope);\n    });\n  } else {\n    //The Service Worker isn't supported on this browser\n    console.error('Este navegador no es compatible con el Service Worker.');\n  }\n  ;\n})();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJuYXZpZ2F0b3IiLCJzZXJ2aWNlV29ya2VyIiwicmVnaXN0ZXIiLCJ0aGVuIiwicmVnaXN0cmF0aW9uIiwiY29uc29sZSIsImxvZyIsInNjb3BlIiwiZXJyb3IiXSwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL25hdmlnYXRvci5qcz85MDdjIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcclxuICAgIFwidXNlIHN0cmljdFwiO1xyXG4gICAgXHJcbiAgICAvLyBJbml0aWFsaXplIFRoZSBTZXJ2aWNlIFdvcmtlci5cclxuICAgIGlmKG5hdmlnYXRvci5zZXJ2aWNlV29ya2VyKSB7XHJcbiAgICAgICAgLy8gUmVnaXN0ZXIgYSBTZXJ2aWNlIFdvcmtlciBob3N0ZWQgYXQgdGhlIHJvb3Qgb2YgdGhlIHNpdGUgdXNpbmcgdGhlIGRlZmF1bHQgc2NvcGUuXHJcbiAgICAgICAgbmF2aWdhdG9yLnNlcnZpY2VXb3JrZXIucmVnaXN0ZXIoJy9zZXJ2aWNld29ya2VyLmpzJykudGhlbigocmVnaXN0cmF0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIFJlZ2lzdHJhdGlvbiB3YXMgc3VjY2Vzc2Z1bC5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0VsIHJlZ2lzdHJvIGRlbCBTZXJ2aWNlIFdvcmtlciBzZSByZWFsaXrDsyBjb3JyZWN0YW1lbnRlOiAnLCByZWdpc3RyYXRpb24uc2NvcGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICAvL1RoZSBTZXJ2aWNlIFdvcmtlciBpc24ndCBzdXBwb3J0ZWQgb24gdGhpcyBicm93c2VyXHJcbiAgICAgICAgY29uc29sZS5lcnJvcignRXN0ZSBuYXZlZ2Fkb3Igbm8gZXMgY29tcGF0aWJsZSBjb24gZWwgU2VydmljZSBXb3JrZXIuJyk7XHJcbiAgICB9O1xyXG59KSgpIl0sIm1hcHBpbmdzIjoiQUFBQSxDQUFDLFlBQVc7RUFDUixZQUFZOztFQUVaO0VBQ0EsSUFBR0EsU0FBUyxDQUFDQyxhQUFhLEVBQUU7SUFDeEI7SUFDQUQsU0FBUyxDQUFDQyxhQUFhLENBQUNDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDQyxJQUFJLENBQUMsVUFBQ0MsWUFBWSxFQUFLO01BQ3pFO01BQ0FDLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLDJEQUEyRCxFQUFFRixZQUFZLENBQUNHLEtBQUssQ0FBQztJQUNoRyxDQUFDLENBQUM7RUFDTixDQUFDLE1BQU07SUFDSDtJQUNBRixPQUFPLENBQUNHLEtBQUssQ0FBQyx3REFBd0QsQ0FBQztFQUMzRTtFQUFDO0FBQ0wsQ0FBQyxHQUFHIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL25hdmlnYXRvci5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/navigator.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/navigator.js"]();
/******/ 	
/******/ })()
;
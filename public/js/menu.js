/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/menu.js":
/*!******************************!*\
  !*** ./resources/js/menu.js ***!
  \******************************/
/***/ (() => {

eval("(function () {\n  \"use strict\";\n\n  var body = document.querySelector('body'),\n    sidebar = body.querySelector('nav'),\n    toggle = body.querySelector(\".toggle\"),\n    searchBtn = body.querySelector(\".search-box\");\n  toggle.addEventListener(\"click\", function () {\n    sidebar.classList.toggle(\"close\");\n  });\n  searchBtn.addEventListener(\"click\", function () {\n    sidebar.classList.remove(\"close\");\n  });\n})();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJib2R5IiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwic2lkZWJhciIsInRvZ2dsZSIsInNlYXJjaEJ0biIsImFkZEV2ZW50TGlzdGVuZXIiLCJjbGFzc0xpc3QiLCJyZW1vdmUiXSwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL21lbnUuanM/M2I1NiJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKSB7XHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgICBjb25zdCBib2R5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpLFxyXG4gICAgICBzaWRlYmFyID0gYm9keS5xdWVyeVNlbGVjdG9yKCduYXYnKSxcclxuICAgICAgdG9nZ2xlID0gYm9keS5xdWVyeVNlbGVjdG9yKFwiLnRvZ2dsZVwiKSxcclxuICAgICAgc2VhcmNoQnRuID0gYm9keS5xdWVyeVNlbGVjdG9yKFwiLnNlYXJjaC1ib3hcIilcclxuXHJcbiAgICB0b2dnbGUuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIgLCAoKSA9PntcclxuICAgICAgICBzaWRlYmFyLmNsYXNzTGlzdC50b2dnbGUoXCJjbG9zZVwiKTtcclxuICAgIH0pXHJcblxyXG4gICAgc2VhcmNoQnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiICwgKCkgPT57XHJcbiAgICAgICAgc2lkZWJhci5jbGFzc0xpc3QucmVtb3ZlKFwiY2xvc2VcIik7XHJcbiAgICB9KVxyXG4gIH0pKCkiXSwibWFwcGluZ3MiOiJBQUFBLENBQUMsWUFBVztFQUNSLFlBQVk7O0VBRVosSUFBTUEsSUFBSSxHQUFHQyxRQUFRLENBQUNDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDekNDLE9BQU8sR0FBR0gsSUFBSSxDQUFDRSxhQUFhLENBQUMsS0FBSyxDQUFDO0lBQ25DRSxNQUFNLEdBQUdKLElBQUksQ0FBQ0UsYUFBYSxDQUFDLFNBQVMsQ0FBQztJQUN0Q0csU0FBUyxHQUFHTCxJQUFJLENBQUNFLGFBQWEsQ0FBQyxhQUFhLENBQUM7RUFFL0NFLE1BQU0sQ0FBQ0UsZ0JBQWdCLENBQUMsT0FBTyxFQUFHLFlBQUs7SUFDbkNILE9BQU8sQ0FBQ0ksU0FBUyxDQUFDSCxNQUFNLENBQUMsT0FBTyxDQUFDO0VBQ3JDLENBQUMsQ0FBQztFQUVGQyxTQUFTLENBQUNDLGdCQUFnQixDQUFDLE9BQU8sRUFBRyxZQUFLO0lBQ3RDSCxPQUFPLENBQUNJLFNBQVMsQ0FBQ0MsTUFBTSxDQUFDLE9BQU8sQ0FBQztFQUNyQyxDQUFDLENBQUM7QUFDSixDQUFDLEdBQUciLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvbWVudS5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/menu.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/menu.js"]();
/******/ 	
/******/ })()
;
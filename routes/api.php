<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [App\Http\Controllers\APIs\UsuarioController::class, 'login'])->name('usuario.login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::apiResources([
        'usuario' => 'App\Http\Controllers\APIs\UsuarioController',
        'estudianteAsistencia' => 'App\Http\Controllers\APIs\EstudianteAsistenciaController',
        'notificacionAula' => 'App\Http\Controllers\APIs\NotificacionAulaController',
    ]);
    Route::post('/logout', [App\Http\Controllers\APIs\UsuarioController::class, 'logout'])->name('usuario.logout');
});
